#!/usr/bin/make -f

UPSTREAM_GIT = https://github.com/openstack/neutron-fwaas.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=python_distutils --with python3

override_dh_auto_clean:
	python3 setup.py clean
	rm -f python3-neutron-fwaas.postinst

override_dh_clean:
	dh_clean
	find . -type f -name "*.pyc" -delete
	rm -rf build

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func python3-neutron-fwaas.postinst

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	set -e ; for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/tmp; \
	done
	rm -rf $(CURDIR)/debian/tmp/usr/etc

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 'neutron_fwaas\.tests\.unit\.(?!.*services\.firewall\.plugins\.cisco\.test_cisco_fwaas_plugin\.TestCiscoFirewallPlugin\.test_update_csr_firewall.*)'
endif

	mkdir -p $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/fwaas_driver.ini \
		--wrap-width 140 \
		--namespace firewall.agent

	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/neutron_fwaas.conf \
		--wrap-width 140 \
		--namespace neutron.fwaas

	# Enable the plugin by default
	pkgos-fix-config-default $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/fwaas_driver.ini fwaas driver iptables_v2
	pkgos-fix-config-default $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/fwaas_driver.ini fwaas enabled True
	pkgos-fix-config-default $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/fwaas_driver.ini fwaas conntrack_driver netlink_conntrack
	pkgos-fix-config-default $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/fwaas_driver.ini fwaas firewall_l2_driver ovs

	pkgos-fix-config-default $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/neutron_fwaas.conf service_providers service_provider FIREWALL_V2:fwaas_db:neutron_fwaas.services.firewall.service_drivers.agents.agents.FirewallAgentDriver:default

	mkdir -p $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/rootwrap.d
	cp etc/neutron/rootwrap.d/fwaas-privsep.filters $(CURDIR)/debian/python3-neutron-fwaas/etc/neutron/rootwrap.d

	dh_install
	dh_missing --fail-missing

override_dh_auto_test:
	echo "Do nothing..."
